<?php

$txt = '{"length":100.0},{"length":100.0},{"length":100.0},{"length":100.0,"switch":true},{"radius":100,"angle":45.0},{"radius":100,"angle":45.0},{"radius":100,"angle":45.0},{"radius":100,"angle":45.0},{"radius":200,"angle":22.5,"switch":true},{"length":100.0},{"length":100.0},{"radius":200,"angle":-22.5},{"length":100.0},{"length":100.0,"switch":true},{"radius":100,"angle":-45.0},{"radius":100,"angle":-45.0},{"radius":100,"angle":-45.0},{"radius":100,"angle":-45.0},{"length":100.0,"switch":true},{"radius":100,"angle":45.0},{"radius":100,"angle":45.0},{"radius":100,"angle":45.0},{"radius":100,"angle":45.0},{"radius":200,"angle":22.5},{"radius":200,"angle":-22.5},{"length":100.0,"switch":true},{"radius":100,"angle":45.0},{"radius":100,"angle":45.0},{"length":62.0},{"radius":100,"angle":-45.0,"switch":true},{"radius":100,"angle":-45.0},{"radius":100,"angle":45.0},{"radius":100,"angle":45.0},{"radius":100,"angle":45.0},{"radius":100,"angle":45.0},{"length":100.0,"switch":true},{"length":100.0},{"length":100.0},{"length":100.0},{"length":90.0}]';

$txtgermany = '{"length":100.0},{"length":100.0,"switch":true},{"radius":50,"angle":45.0},{"radius":50,"angle":45.0},{"radius":50,"angle":45.0},{"length":50.0},{"radius":50,"angle":-45.0},{"radius":50,"angle":-45.0},{"radius":50,"angle":-45.0},{"length":50.0},{"radius":50,"angle":-45.0},{"radius":50,"angle":-45.0},{"radius":100,"angle":-22.5},{"radius":50,"angle":45.0},{"length":100.0,"switch":true},{"length":100.0},{"radius":50,"angle":-45.0},{"length":50.0},{"radius":50,"angle":45.0},{"radius":50,"angle":45.0},{"radius":100,"angle":22.5},{"length":100.0,"switch":true},{"length":100.0},{"radius":50,"angle":45.0},{"radius":50,"angle":45.0},{"radius":50,"angle":45.0},{"radius":50,"angle":45.0},{"length":100.0,"switch":true},{"radius":50,"angle":-45.0},{"radius":50,"angle":-45.0},{"length":50.0},{"radius":100,"angle":22.5},{"length":100.0},{"length":50.0},{"radius":100,"angle":-22.5},{"radius":100,"angle":-22.5},{"length":100.0,"switch":true},{"radius":50,"angle":45.0},{"radius":50,"angle":45.0},{"radius":100,"angle":22.5},{"length":100.0},{"length":100.0},{"length":100.0},{"radius":100,"angle":45.0},{"length":70.0},{"length":100.0,"switch":true},{"radius":50,"angle":-45.0},{"radius":50,"angle":-45.0},{"radius":100,"angle":45.0},{"radius":100,"angle":45.0},{"length":50.0},{"radius":100,"angle":45.0},{"radius":100,"angle":45.0},{"radius":100,"angle":45.0,"switch":true},{"length":100.0},{"length":100.0},{"length":59.0}';

$txtusa = '{"length":100.0},{"length":100.0,"switch":true},{"radius":200,"angle":22.5},{"radius":200,"angle":22.5},{"radius":200,"angle":22.5},{"radius":200,"angle":22.5},{"length":100.0},{"length":100.0,"switch":true},{"radius":200,"angle":22.5},{"radius":200,"angle":22.5},{"radius":200,"angle":22.5},{"radius":200,"angle":22.5},{"length":100.0},{"length":100.0},{"length":100.0},{"length":100.0},{"length":100.0},{"length":100.0,"switch":true},{"radius":200,"angle":22.5},{"radius":200,"angle":22.5},{"radius":200,"angle":22.5},{"radius":200,"angle":22.5},{"length":100.0},{"length":100.0,"switch":true},{"radius":200,"angle":22.5},{"radius":200,"angle":22.5},{"radius":200,"angle":22.5},{"radius":200,"angle":22.5},{"length":100.0},{"length":100.0},{"length":100.0},{"length":100.0}';

$txt = '{"msgType":"gameInit","data":{"race":{"track":{"id":"keimola","name":"Keimola","pieces":[{"length":100.0},{"length":100.0},{"length":100.0},{"length":100.0,"switch":true},{"radius":100,"angle":45.0},{"radius":100,"angle":45.0},{"radius":100,"angle":45.0},{"radius":100,"angle":45.0},{"radius":200,"angle":22.5,"switch":true},{"length":100.0},{"length":100.0},{"radius":200,"angle":-22.5},{"length":100.0},{"length":100.0,"switch":true},{"radius":100,"angle":-45.0},{"radius":100,"angle":-45.0},{"radius":100,"angle":-45.0},{"radius":100,"angle":-45.0},{"length":100.0,"switch":true},{"radius":100,"angle":45.0},{"radius":100,"angle":45.0},{"radius":100,"angle":45.0},{"radius":100,"angle":45.0},{"radius":200,"angle":22.5},{"radius":200,"angle":-22.5},{"length":100.0,"switch":true},{"radius":100,"angle":45.0},{"radius":100,"angle":45.0},{"length":62.0},{"radius":100,"angle":-45.0,"switch":true},{"radius":100,"angle":-45.0},{"radius":100,"angle":45.0},{"radius":100,"angle":45.0},{"radius":100,"angle":45.0},{"radius":100,"angle":45.0},{"length":100.0,"switch":true},{"length":100.0},{"length":100.0},{"length":100.0},{"length":90.0}],"lanes":[{"distanceFromCenter":-10,"index":0},{"distanceFromCenter":10,"index":1}],"startingPoint":{"position":{"x":-300.0,"y":-44.0},"angle":90.0}},"cars":[{"id":{"name":"speedbooster","color":"red"},"dimensions":{"length":40.0,"width":20.0,"guideFlagPosition":10.0}}],"raceSession":{"laps":3,"maxLapTimeMs":60000,"quickRace":true}}},"gameId":"44f12310-8977-45e1-880c-bc78066703f5"}';


$msgd = json_decode($txt, TRUE);
$pieces = $msgd['data']['race']['track']['pieces'];
for ($i = 0; $i<count($pieces); $i++) {
	$pid = $i;
	if ($pieces[$i]['length'] > 0) {
		echo "length:\t" . $pieces[$i]['length'];
		if ($pieces[$i]['switch'] == 1) {
			echo "\t\t\t" . "<<";
		}
		echo "\n\n";
	}
	else {
		echo "radius:\t" . $pieces[$i]['radius'] . "\n";
		echo "angle:\t" . $pieces[$i]['angle'] . "";
		if ($pieces[$i]['switch'] == 1) {
			echo "\t\t" . "<<";
		}
		echo "\n\n";
	}

}











?>
