<?php
define('MAX_LINE_LENGTH', 1024 * 1024);
date_default_timezone_set('Asia/Karachi');
//require_once 'json.php';

class BasicBot {
	protected $sock, $debug, $log, $file, $timestamp;
	protected $throttle, $myname, $mycolor, $prevangle;

	function __construct($host, $port, $botname, $botkey, $debug = TRUE) {
		$this->debug = $debug;
		$this->timestamp = date("Y_m_d__a_h_i_s");
		$this->throttle = 1.0;
		$this->prevangle = 0;
//echo $botkey;
		$this->log = "logs/log_" . $this->timestamp . ".txt";
		$this->connect($host, $port, $botkey);

		$this->write_msg('join', array(
			'name' => $botname,
			'key' => $botkey
		));
/*
		$this->write_msg('joinRace', array(
			'botId' => array(
				'name' => $botname,
				'key' => $botkey
			),
			'trackName' => 'keimola',
			'password' => 'asdf',
			'carCount' => 1
		));
*/

		$this->file=fopen($this->log,"w");
	}

	function __destruct() {
		if (isset($this->sock)) {
			socket_close($this->sock);
		fclose($this->file);
		}
	}

	protected function connect($host, $port, $botkey) {
		$this->sock = @ socket_create(AF_INET, SOCK_STREAM, SOL_TCP);
		if ($this->sock === FALSE) {
			throw new Exception('socket: ' . socket_strerror(socket_last_error()));
		}
		if (@ !socket_connect($this->sock, $host, $port)) {
			throw new Exception($host . ': ' . $this->sockerror());
		}
	}

	protected function read_msg() {
		$line = @ socket_read($this->sock, MAX_LINE_LENGTH, PHP_NORMAL_READ);
		if ($line === FALSE) {
			$this->debug('** ' . $this->sockerror());
		} else {
			$this->debug('<= ' . rtrim($line));
			
			fwrite($this->file, rtrim($line) . "\n");


		}
		return json_decode($line, TRUE);
	}

	protected function write_msg($msgtype, $data, $gtick=null) {
//echo $data;
	if (strlen($gtick) > 0) {
		$str = json_encode(array('msgType' => $msgtype, 'data' => $data, 'gameTick' => $gtick)) . "\n";
	}
	else {
		$str = json_encode(array('msgType' => $msgtype, 'data' => $data)) . "\n";
	}
		$this->debug('=> ' . rtrim($str));
		if (@ socket_write($this->sock, $str) === FALSE) {
			throw new Exception('write: ' . $this->sockerror());
		}
	}
	
	protected function sockerror() {
		return socket_strerror(socket_last_error($this->sock));
	}
	
	protected function debug($msg) {
		if ($this->debug) {
			echo $msg, "\n";
		}
	}
	
	public function run() {

		function comp($a, $b) {
			$r = 0;
			if ($a['stamp'] < $b['stamp']) {
				$r = -1;
			}
			elseif ($a['stamp'] > $b['stamp']) {
				$r = 1;
			}
			return $r;
		}


		while (!is_null($msg = $this->read_msg())) {

$ping = 1;
$msgd = $msg;

switch ($msgd["msgType"]) {
	
	// ******************************************************************************
	
	case "join":
	echo "*** Its Join !";
	echo "\r\n";

	echo "Name:			";
	echo $joinname = $msgd['data']['name'];
	echo "\r\n";
	echo "Color:			";
	echo $joinkey = $msgd['data']['key'];
	echo "\r\n";

	$games = 0;

	break;
	
	// ******************************************************************************
	
	case "gameInit":
	echo "*** its init";
	echo "\r\n";
	
	$f = fopen("tracks/position_" . $this->timestamp . ".txt", "w");


	$throttle = 1.0;

	$prev_piece = 0;
	$current_piece = 0;

	$delta = 0;
	$ddelta = 0;
	$prev_delta = 0;


	$turbo = 0;
	$turbine = 0;
	$turbined = 0;
	$turbotimer = 0;
	$turboduration = 0;

	$turbothrottle = 0.2;
	$deltathrottle = 0;
	$prev_throttle = $throttle;

	$prev_angle = 0;
	$prev_dangle = 0;
	$prev_ddangle = 0;


	$prev_distance = 0;	// Change it to starting position ?? NO, its my 'elapsed' distance, not absolute. Not really??
	$tdistance = 0;



if ($games == 0) {


	$longestid = -1;
	$longestlength = 0;
	$longestbuffer = 0;
	$longestbufferid = -1;
	$longestbreak = 1;

	$radialid = -1;
	$radiallength = 0;
	$radialbuffer = 0;
	$radialbufferid = -1;
	$radialbreak = 1;


	// ** Don't wait for gameStart command, arrives late.

	$this->write_msg('throttle', $throttle);

	echo "Track ID:			";
	echo $trackid = $msgd['data']['race']['track']['id'];
	echo "\r\n";
	echo "Track name:		";
	echo $trackname = $msgd['data']['race']['track']['name'];
	echo "\r\n";
	echo "Track pieces:		";
	echo $trackpiecescount = count($msgd['data']['race']['track']['pieces']);
	$trackpieces = $msgd['data']['race']['track']['pieces'];

	echo "\r\n";
	echo "Track lanes:		";
	echo $tracklanescount = count($msgd['data']['race']['track']['lanes']);
	$tracklanes = $msgd['data']['race']['track']['lanes'];
	$maxlane = 0;

	for ($i = 0; $i<count($tracklanes); $i++) {
		$lanes[$i][0] = $tracklanes[$i]['index'];		// better to use this than rely on 'i' or server to send it in 'order'
		$lanes[$i][1] = $tracklanes[$i]['distanceFromCenter'];	// distance from center
		if (abs($tracklanes[$i]['distanceFromCenter']) > $maxlane) {
			$maxlane = abs($tracklanes[$i]['distanceFromCenter']);
		}
	}




	$j = 0;
	$l = 0;
/*
	$tracklist[0]['type'] = 0;		// type
	$tracklist[0]['startid'] = 0;		// type
	$tracklist[0]['length'] = 0;		// Length/ CIrcumference
	$tracklist[0]['tlength'] = 0;		// Length/ CIrcumference
	$tracklist[0]['angle'] = 0;		// Angle
	$tracklist[0]['switch'] = 0;		// switch
	$tracklist[0]['radius'] = 0;		// radius
	$tracklist[0]['turnfactor'] = 0;		// turn factor
	$tracklist[0]['patchmultiplier'] = 0;		// turn multiplier (no. of consecutive patches)
	$tracklist[0]['tturnfactor'] = 0;		// total incremental accumulated turn factor
	$tracklist[0]['tgturnfactor'] = 0;		// total group turn factor
	$tracklist[0]['tglength'] = 0;			// total group length
	$tracklist[0]['lastid'] = 0;			// last id/ id of last patch
	$tracklist[0]['aspeed'] = 0;			// Optimal speed
*/

	for ($i = 0; $i<count($trackpieces); $i++) {
		if ($trackpieces[$i]['length'] > 0) {

			$pieces[$i][0] = 0; 				// 0 for straight
			$pieces[$i][1] = $trackpieces[$i]['length']; 	// length
			$pieces[$i][2] = 0;				// dummy entry for angle
			if ($trackpieces[$i]['switch'] == 1) {
				$pieces[$i][3] = 1; 			// switchability 0 = false, 1 = true
			}
			else {
				$pieces[$i][3] = 0; 			// switchability 0 = false, 1 = true
			}
			$pieces[$i][4] = 0;				// dummy entry for radius

			$tracklist[$i]['type'] = 0;			//
			$tracklist[$i]['startid'] = $i;			//
			$tracklist[$i]['length'] = $pieces[$i][1];	//
			$tracklist[$i]['tlength'] = $pieces[$i][1];
			$tracklist[$i]['angle'] = 0;			// ...
			$tracklist[$i]['switch'] = $pieces[$i][3];	// switch
			$tracklist[$i]['radius'] = 0;			// ...
			$tracklist[$i]['turnfactor'] = 0;		// ...
			$tracklist[$i]['patchmultiplier'] = 1;		// ... can be used as turn multiplier as well as patch multiplier
			$tracklist[$i]['tturnfactor'] = 0;
			$tracklist[$i]['tgturnfactor'] = 0;		// ...
			$tracklist[$i]['tglength'] = $pieces[$i][1];
			$tracklist[$i]['lastid'] = $i;
			$tracklist[$i]['aspeed'] = 5 + (($tracklist[$i]['tglength']/100)*0.5);	// Optimal speed

			if ($longestbreak == 0) {
				$longestbuffer = $longestbuffer + $trackpieces[$i]['length'];
				for ($k=$j; $k<$i; $k++) {
					$tracklist[$k]['patchmultiplier'] = $i - $j + 1;
					$tracklist[$k]['tlength'] = $tracklist[$k]['tlength'] + $pieces[$i][1];
					$tracklist[$k]['tglength'] = $tracklist[$j]['tlength'];
					$tracklist[$k]['lastid'] = $i;
					$tracklist[$k]['aspeed'] = 5 + (($tracklist[$k]['tglength']/100)*0.5);
				}
				$tracklist[$i]['startid'] = $j;
				$tracklist[$i]['patchmultiplier'] = $i - $j + 1;
				$tracklist[$i]['tglength'] = $tracklist[$j]['tlength'];
				$tracklist[$i]['aspeed'] = 5 + (($tracklist[$i]['tglength']/100)*0.5);
			}
			else {
				$j = $i;
				$longestbufferid = $i;		// New id in buffer
				$longestbuffer = $trackpieces[$i]['length'];	// buffer accumulate
				$longestbreak = 0;		// continuity begins
			}
			if ($longestbuffer > $longestlength) {
				$longestid = $longestbufferid;
				$longestlength = $longestbuffer;
			}
			$radialbreak = 1;
		}
		else {
			$pieces[$i][0] = 1; 				// 1 for turn
			$pieces[$i][1] = abs(($trackpieces[$i]['radius']+$maxlane)*$trackpieces[$i]['angle']*3.1415/180); 	// circumference
			$pieces[$i][2] = $trackpieces[$i]['angle'];	// angle
			if ($trackpieces[$i]['switch'] == 1) {
				$pieces[$i][3] = 1; 			// switchability 0 = false, 1 = true
			}
			else {
				$pieces[$i][3] = 0; 			// switchability 0 = false, 1 = true
			}
			$pieces[$i][4] = $trackpieces[$i]['radius'];	// Radius

			$tracklist[$i]['type'] = 1;			// 
			$tracklist[$i]['startid'] = $i;			// Start id
			$tracklist[$i]['length'] = $pieces[$i][1];	// 
			$tracklist[$i]['tlength'] = $pieces[$i][1];
			$tracklist[$i]['angle'] = $pieces[$i][2];	// ...
			$tracklist[$i]['switch'] = $pieces[$i][3];	// switch
			$tracklist[$i]['radius'] = $pieces[$i][4];
			$tracklist[$i]['turnfactor'] = $pieces[$i][2]/$pieces[$i][4];
			$tracklist[$i]['patchmultiplier'] = 1;
			$tracklist[$i]['tturnfactor'] = $pieces[$i][2]/$pieces[$i][4];
			$tracklist[$i]['tgturnfactor'] = $pieces[$i][2]/$pieces[$i][4];		// ...
			$tracklist[$i]['tglength'] = $pieces[$i][1];
			$tracklist[$i]['lastid'] = $i;
			$tracklist[$i]['aspeed'] = 5 - abs($tracklist[$i]['tgturnfactor']/2);	// Optimal speed

			if ($radialbreak == 0) { //DOnt merge with below code
				if (($pieces[$i][2]*$pieces[$i-1][2]) < 0) {	//Same side corner, no matter what angle. could equate angles and radiuses
					$radialbreak = 1;
				}
			}
			if ($radialbreak == 0) {
				$radialbuffer = $radialbuffer + $trackpieces[$i]['length'];
				$tfacc = 0;
				for ($k=$l; $k<$i; $k++) {
					$tracklist[$k]['tlength'] = $tracklist[$k]['tlength'] + $pieces[$i][1];
					$tracklist[$k]['patchmultiplier'] = $i - $l + 1;
					$tracklist[$k]['tturnfactor'] = $tracklist[$k]['tturnfactor'] + $tracklist[$i]['turnfactor'];
					$tracklist[$k]['tglength'] = $tracklist[$l]['tlength'];
					$tracklist[$k]['tgturnfactor'] = $tracklist[$l]['tturnfactor'];
					$tracklist[$k]['lastid'] = $i;
					$tracklist[$k]['aspeed'] = 5 - abs($tracklist[$k]['tgturnfactor']/2);
				}
				$tracklist[$i]['startid'] = $l;
				$tracklist[$i]['tglength'] = $tracklist[$l]['tlength'];
				$tracklist[$i]['tgturnfactor'] = $tracklist[$l]['tturnfactor'];
				$tracklist[$i]['patchmultiplier'] = $i - $l + 1;
				$tracklist[$i]['aspeed'] = 5 - abs($tracklist[$i]['tgturnfactor']/2);
			}
			else {
				$l = $i;
				$radialbufferid = $i;				// New id in buffer
				$radialbuffer = $trackpieces[$i]['length'];	// buffer accumulate
				$radialbreak = 0;				// continuity begins
			}
			if ($radialbuffer > $radiallength) {
				$radialid = $radialbufferid;
				$radiallength = $radialbuffer;
			}
			$longestbreak = 1;
		}
	}
	if ($tracklist[0]['type'] == $tracklist[$trackpiecescount-1]['type']) { // joining circular path at finish line
		if ($tracklist[0]['type'] == 0) { // joining circular path at finish line
			$sindex = $j;
		}
		else {
			$sindex = $l;
		}
		for ($k=0; $k<=$tracklist[0]['lastid']; $k++) {
			$tracklist[$k]['tglength'] = $tracklist[$k]['tglength'] + $tracklist[$j]['tlength'];
			$tracklist[$k]['tgturnfactor'] = $tracklist[$k]['tgturnfactor'] + $tracklist[$j]['tturnfactor'];
			$tracklist[$k]['patchmultiplier'] = $tracklist[$k]['patchmultiplier'] + ($trackpiecescount - $j);
			if ($tracklist[$k]['type'] == 1) {
				$tracklist[$k]['aspeed'] = 5 - abs($tracklist[$k]['tgturnfactor']/2);
			}
			else {
				$tracklist[$k]['aspeed'] = 5 + (($tracklist[$k]['tglength']/100)*0.5);
			}
		}
		for ($k=$sindex; $k<=($trackpiecescount-1); $k++) {
			$tracklist[$k]['tglength'] = $tracklist[0]['tglength'];
			$tracklist[$k]['tlength'] = $tracklist[$k]['tlength'] + $tracklist[0]['tlength'];
			$tracklist[$k]['tturnfactor'] = $tracklist[$k]['tturnfactor'] + $tracklist[0]['tturnfactor'];
			$tracklist[$k]['tgturnfactor'] = $tracklist[0]['tturnfactor'];
			$tracklist[$k]['lastid'] = $tracklist[0]['lastid'];
			$tracklist[$k]['patchmultiplier'] = $tracklist[$k]['patchmultiplier'] + ($tracklist[0]['lastid'] + 1);
			if ($tracklist[$k]['type'] == 1) {
				$tracklist[$k]['aspeed'] = 5 - abs($tracklist[$k]['tgturnfactor']/2);
			}
			else {
				$tracklist[$k]['aspeed'] = 5 + (($tracklist[$k]['tglength']/100)*0.5);
			}
		}
		if ($tracklist[0]['type'] == 0) {
			if ($tracklist[0]['tglength'] > $longestlength) {
				$longestid = $j;
				$longestlength = $tracklist[0]['tglength'];
			}
		}
	}

}
	$games = $games + 1;

	// compute maximum straight stretch here; --  computed???


	
	echo "\r\n";
	echo "Starting pointx:		";
	echo $trackstartingpointx = $msgd['data']['race']['track']['startingPoint']['position']['x'];
	echo "\r\n";
	echo "Starting pointy:		";
	echo $trackstartingpointy = $msgd['data']['race']['track']['startingPoint']['position']['y'];
	echo "\r\n";
	echo "Starting angle:		";
	echo $trackstartingpointangle = $msgd['data']['race']['track']['startingPoint']['angle'];
	echo "\r\n";
	echo "Cars:			";
	echo $carscount = count($msgd['data']['race']['cars']);
	$trackcars = $msgd['data']['race']['cars'];

	for ($i = 0; $i<count($trackcars); $i++) {
		$cars[$i][0] = $trackcars[$i]['id']['name'];
		$cars[$i][1] = $trackcars[$i]['id']['color'];
		$cars[$i][2] = $trackcars[$i]['dimensions']['length'];
		$cars[$i][3] = $trackcars[$i]['dimensions']['width'];
		$cars[$i][4] = $trackcars[$i]['dimensions']['guideFlagPosition'];
		if (($cars[$i][0] == $myname) && ($cars[$i][1] == $mycolor)) {
			$this->mycarsid = $i;
		}
	}

	echo "\r\n";
	echo "Laps:			";
	echo $totallaps = $msgd['data']['race']['raceSession']['laps'];
	echo "\r\n";
	echo "Max time limit:		";
	echo $maxlaptime = $msgd['data']['race']['raceSession']['maxLapTimeMs'];
	echo "\r\n";
	echo "Quick race?:		";
	echo $quickrace = $msgd['data']['race']['raceSession']['quickRace'];
	echo "\r\n";
	
	break;

	// ******************************************************************************
		
	case "yourCar":
	echo "*** Its Your Car";
	echo "\r\n";
	
	echo "Car name:			";
	echo $myname = $msgd['data']['name'];
	echo "\r\n";
	echo "Car Color:		";
	echo $mycolor = $msgd['data']['color'];

	$this->myname = $myname;
	$this->mycolor = $mycolor;
	
	break;

	// ******************************************************************************

	case "gameStart":
	echo "*** Its Game Start !!";
	echo "\r\n";
	$throttle = 1.0;
	$this->write_msg('throttle', $throttle);
	break;

	// ******************************************************************************

	case "carPositions":
	echo "*** Its Car Positions";
	echo "\r\n";
	
	echo "Game ID:\t\t";
	echo $gameid = $msgd['gameId'];
	echo "\r\n";
	echo "Game tick:\t\t";
	echo $gametick = $msgd['gameTick'];
	echo "\r\n";

	echo "Count data Cars:\t";
	echo $carspositionscount = count($msgd['data']);
	$carspositions = $msgd['data'];
	echo "\r\n";
	
	// Extract personal car position, extract other car position;
	
	echo "\r\n";


//********************************************************
	$posid = 0;
/*
	$carslist[0]['posid'] = -1;
	$carslist[0]['name'] = "";
	$carslist[0]['color'] = "";
	$carslist[0]['angle'] = 0;
	$carslist[0]['piece'] = 0;
	$carslist[0]['inpiecedist'] = 0;
	$carslist[0]['startlane'] = 0;
	$carslist[0]['endlane'] = 0;
	$carslist[0]['lap'] = 0;
	$carslist[0]['stamp'] = 0;
	$carslist[0]['delta'] = 0;
	$carslist[0]['prev_piece'] = 0;
	$carslist[0]['prev_distance'] = 0;
	$carslist[0]['prev_delta'] = 0;
	$carslist[0]['ddelta'] = 0;
*/
for ($j=0; $j<$carspositionscount; $j++) {
	if ($gametick > 1) {
		for ($i=0; $i<$carspositionscount; $i++) {
			if (($carslist[$i]['name'] == $msgd['data'][$j]['id']['name']) && ($carslist[$i]['color'] == $msgd['data'][$j]['id']['color'])) {
				//	$carslist[$i]['name'] = $msgd['data'][$i]['id']['name'];
				//	$carslist[$i]['color'] = $msgd['data'][$i]['id']['color'];
				$carslist[$i]['angle'] = $msgd['data'][$j]['angle'];
				$carslist[$i]['piece'] = $msgd['data'][$j]['piecePosition']['pieceIndex'];
				$carslist[$i]['inpiecedist'] = $msgd['data'][$j]['piecePosition']['inPieceDistance'];
				$carslist[$i]['startlane'] = $msgd['data'][$j]['piecePosition']['lane']['startLaneIndex'];
				$carslist[$i]['endlane'] = $msgd['data'][$j]['piecePosition']['lane']['endLaneIndex'];
				$carslist[$i]['lap'] = $msgd['data'][$j]['piecePosition']['lap'];
				$carslist[$i]['stamp'] = ($carslist[$i]['lap']+2) . $carslist[$i]['piece'] . $carslist[$i]['inpiecedist'];
				// Adding 2 to lap 0 to avoid 0 issue

				if ($gametick == 1) {
					$carslist[$i]['prev_piece'] = $carslist[$i]['piece'];
					$carslist[$i]['prev_distance'] = $carslist[$i]['inpiecedist'];
					$carslist[$i]['prev_delta'] = 0;
					$carslist[$i]['delta'] = 0;
					$carslist[$i]['ddelta'] = 0;
				}

				if ($carslist[$i]['piece'] == $carslist[$i]['prev_piece']) {
					$carslist[$i]['delta'] = $carslist[$i]['inpiecedist'] - $carslist[$i]['prev_distance'];
					$carslist[$i]['ddelta'] = $carslist[$i]['delta'] - $carslist[$i]['prev_delta'];
					//$tdistance = $tdistance + ($inpiecedist - $prev_distance);
				}
				else {	// Dont compute delta
					// $carslist[$i]['delta'] = $carslist[$i]['inpiecedist'] - $carslist[$i]['prev_distance'];
		
				}
				/*
				else {
					if ($carslist[$i]['piece'] == 0) { // new lap started
						//$tdistance = $tdistance + $pieces[$trackpiecescount-1][1] - $prev_distance + $inpiecedist;
						//	$delta = $inpiecedist + ($pieces[$trackpiecescount-1][1] - $prev_distance);
					}
					else {
						//$tdistance = $tdistance + $pieces[$prev_piece][1] - $prev_distance + $inpiecedist;
						//	Results in error !!
						//	$delta = $inpiecedist + ($pieces[$prev_piece][1] - $prev_distance);
					}
				}*/
		
				$carslist[$i]['prev_distance'] = $carslist[$i]['inpiecedist'];
				$carslist[$i]['prev_piece'] = $carslist[$i]['piece'];
				$carslist[$i]['prev_delta'] = $carslist[$i]['delta'];
			}
		}
	}
	elseif ($gametick == 1) {
		$carslist[$j]['name'] = $msgd['data'][$j]['id']['name'];
		$carslist[$j]['color'] = $msgd['data'][$j]['id']['color'];
		$carslist[$j]['angle'] = $msgd['data'][$j]['angle'];
		$carslist[$j]['piece'] = $msgd['data'][$j]['piecePosition']['pieceIndex'];
		$carslist[$j]['inpiecedist'] = $msgd['data'][$j]['piecePosition']['inPieceDistance'];
		$carslist[$j]['startlane'] = $msgd['data'][$j]['piecePosition']['lane']['startLaneIndex'];
		$carslist[$j]['endlane'] = $msgd['data'][$j]['piecePosition']['lane']['endLaneIndex'];
		$carslist[$j]['lap'] = $msgd['data'][$j]['piecePosition']['lap'];
		$carslist[$j]['stamp'] = ($carslist[$j]['lap']+2) . $carslist[$j]['piece'] . $carslist[$j]['inpiecedist'];
		// Adding 2 to lap 0 to avoid 0 issue
		if ($gametick == 1) {
			$carslist[$j]['prev_piece'] = $carslist[$j]['piece'];
			$carslist[$j]['prev_distance'] = $carslist[$j]['inpiecedist'];
			$carslist[$j]['prev_delta'] = 0;
			$carslist[$j]['delta'] = 0;
			$carslist[$j]['ddelta'] = 0;
		}
		if ($carslist[$j]['piece'] == $carslist[$j]['prev_piece']) {
			$carslist[$j]['delta'] = $carslist[$j]['inpiecedist'] - $carslist[$j]['prev_distance'];
			$carslist[$j]['ddelta'] = $carslist[$j]['delta'] - $carslist[$j]['prev_delta'];
			//$tdistance = $tdistance + ($inpiecedist - $prev_distance);
		}
		else {	// Dont compute delta
			// $carslist[$i]['delta'] = $carslist[$i]['inpiecedist'] - $carslist[$i]['prev_distance'];

		}
		/*
		else {
			if ($carslist[$i]['piece'] == 0) { // new lap started
				//$tdistance = $tdistance + $pieces[$trackpiecescount-1][1] - $prev_distance + $inpiecedist;
				//	$delta = $inpiecedist + ($pieces[$trackpiecescount-1][1] - $prev_distance);
			}
			else {
				//$tdistance = $tdistance + $pieces[$prev_piece][1] - $prev_distance + $inpiecedist;
				//	Results in error !!
				//	$delta = $inpiecedist + ($pieces[$prev_piece][1] - $prev_distance);
			}
		}*/

		$carslist[$j]['prev_distance'] = $carslist[$j]['inpiecedist'];
		$carslist[$j]['prev_piece'] = $carslist[$j]['piece'];
		$carslist[$j]['prev_delta'] = $carslist[$j]['delta'];
	}

}

	usort($carslist, 'comp');
//	print_r($carslist);

	for ($i=0; $i<$carspositionscount; $i++) {
		if (($carslist[$i]['name'] == $this->myname) && ($carslist[$i]['color'] == $this->mycolor)) {
			$posid = $i;
		}
	}


	for ($i=0; $i<$carspositionscount; $i++) {
		if ($posid == $i) {
			echo "->->->-> \t" . $carslist[$i]['name'] . "\t\t" . $carslist[$i]['delta'] . "\r\n";
		}
		else {
			echo "->-> \t\t" . $carslist[$i]['name'] . "\t\t" . $carslist[$i]['delta'] . "\r\n";
		}
	}


	/*
		if (($this->prevangle == 0) && ($msgd['data'][$posid]['angle'] > 0)) {
		$throttle = 0.01; //Negative feedback, reduce fast, increase slow
		if ($throttle < 0.0) {
			$throttle = 0.1; //Avoid standoff !!
		}
	}*/
	fwrite($this->file, "\n\n" . ">>>>>>>>>>>>>>>>>>>>>>>>>>>>> " . $carslist[$posid]['piece'] . "\n\n");



/*
	if (($this->prevangle - $carslist[$posid]['angle']) < 0) {
		$throttle = $throttle - 0.2; //Negative feedback, reduce fast, increase slow
		if ($throttle < 0.65) {
			$throttle = 0.65; //Avoid standoff !!
		}
	}
	else {
		$throttle = $throttle + 0.1;
		if ($throttle > 0.7) {
			$throttle = 0.65;
		}
	}
	}
*/
//************************************************************************


$inpiecedist = $carslist[$posid]['inpiecedist'];
$lap = $carslist[$posid]['lap'];
$lane = $carslist[$posid]['startlane'];
$current_piece = $carslist[$posid]['piece'];
if ($prev_piece == $current_piece) {
	$delta = $inpiecedist - $prev_distance;
	$tdistance = $tdistance + ($inpiecedist - $prev_distance);
}
else {
	if ($current_piece == 0) { // new lap started
		$tdistance = $tdistance + $pieces[$trackpiecescount-1][1] - $prev_distance + $inpiecedist;
//		$delta = $inpiecedist + ($pieces[$trackpiecescount-1][1] - $prev_distance);
	}
	else {
		$tdistance = $tdistance + $pieces[$prev_piece][1] - $prev_distance + $inpiecedist;
//		Results in error !!
//		$delta = $inpiecedist + ($pieces[$prev_piece][1] - $prev_distance);
	}
}
$prev_distance = $inpiecedist;

$ddelta = $delta - $prev_delta;

// could do it just once every piece not iterative, but would need to fix for first piece, first lap...
// computing current piece length

	$distance_left = $tracklist[$current_piece]['tlength'] - $inpiecedist;

	$distance_elapsed = $tracklist[$current_piece]['tglength'] - $distance_left;
	$pdistance_elapsed = ($tracklist[$current_piece]['tglength'] - $distance_left)/$tracklist[$current_piece]['tglength'];

	if ($current_piece < ($trackpiecescount - 2)) {
		$next_piece = $current_piece + 1;
	}
	else {
		$next_piece = 0;
	}
	if ($tracklist[$current_piece]['lastid'] < ($trackpiecescount - 2)) {
		$next_group = $tracklist[$current_piece]['lastid'] + 1;
	}
	else {
		$next_group = 0;
	}

// new logic
	$switch = 0;


	$straightstretch = 0;
	$nstraightstretch = 0;
	if ($tracklist[$current_piece]['type'] == 0) {
		$straightstretch = $tracklist[$current_piece]['tlength'] - $inpiecedist;
	}
	if ($tracklist[$next_group]['type'] == 0) {
		$nstraightstretch = $tracklist[$next_group]['tlength'];
	}

	if ($switchrequest == 1) {
		if ($switched == 1) {
			$switchrequest = 0;
		}
	}



	if (($current_piece != $prev_piece) || ($switchrequest == 1)) {
	$switched = 0;
//	if ($switcher == 1) {

	$turnfound = 0;
	$turnfound2 = 0;
	$turnid = 0;		// flawed near end of lap
	$turnid2 = 0;		// flawed near end of lap
	$switchfound = 0;
	$switchfound2 = 0;
	$turnbreak = 1;
	$turntimes1 = 0;
	$turntimes2 = 0;

	$i = 0;

	for ($j=($current_piece); $j<($current_piece+7); $j++) {
		if ($i < $trackpiecescount) {
			$i = $j;
		}
		else {
			$i = $j - $trackpiecescount;
		}

		if (($tracklist[$i]['switch'] == 1) && ($turnfound == 0)) { // switch found before/at turn
			$switchfound = $switchfound + 1;
		}

		if (($tracklist[$i]['switch'] == 1) && ($turnfound == 1) && ($turnfound2 == 0)) {
			// switch2 found before/at turn2 could be found at next patch of turn1, no issue
			$switchfound2 = $switchfound2 + 1;
		}

		if ($turnfound == 1) { // be careful where to put it
			if ($turnbreak == 0) {
				$turntimes1 = $turntimes1 + 1;
			}
		}
		if ($turnfound == 2) { // be careful where to put it
			if ($tracklist[$i]['switch'] == 0) {
				$turnbreak = 1;		// measure second turn only till switch after second turn to reduce weight comparing it with turn 1
			}
			if ($turnbreak == 0) {
				$turntimes2 = $turntimes2 + 1;
			}
		}

		if ($turnfound == 0) {
			if ($tracklist[$i]['type'] == 1) {
				$turnfound = 1;
				$turnid = $i;
				$turnbreak = 0;
			}
		}


		if (($turnfound == 1) && ($tracklist[$i]['angle']*$tracklist[$turnid]['angle'] <= 0)) {
			$turnbreak = 1;
		}
		if (($turnfound == 1) && ($switchfound == 1) && ($turnbreak == 1)) {
			if ($tracklist[$i]['type'] == 1) {
				$turnfound = 2;
				$turnid2 = $i;
			}
		}
	}
		if (($switchfound > 0) && ($turnfound == 0)) { // fails for consecutive patches. need accumulated turn logic
			if ($switchrequest == 1) {
				if ($lane < ($tracklanescount - 1)) {
					$switch = 1; // RIght
				}
				else {
					$switch = 2; // Left
				}
			}
		}

		if (($switchfound > 0) && ($turnfound > 0)) { // fails for consecutive patches. need accumulated turn logic
//			$switcher = 0;
		if ($turnfound == 1) {
			if ($tracklist[$turnid]['angle'] > 0) {  // check angle
				$switch = 1;
			}
			else {
				$switch = 2;
			}
		}
		if (($turnfound == 2) && ($switchfound2 == 0)) {
//			if >> dont use (($tracklist[$turnid]['tturnfactor'])+($tracklist[$turnid2]['tturnfactor'])) > 0) {
			if (((($turntimes1*$pieces[$turnid][2])/$pieces[$turnid][1])+(($turntimes2*$pieces[$turnid2][2])/$pieces[$turnid2][1])) > 0) {  //
				$switch = 1;
			}
			// >> dont use turntimes is important elseif (((($tracklist[$turnid]['turnfactor'])+($tracklist[$turnid2]['tturnfactor'])) == 0) {
			elseif (((($turntimes1*$pieces[$turnid][2])/$pieces[$turnid][1])+(($turntimes2*$pieces[$turnid2][2])/$pieces[$turnid2][1])) == 0)
			{
				if ($pieces[$turnid][2] > 0) {  // check angle
					$switch = 1;
				}
				else {
					$switch = 2;
				}
			}
			else {
				$switch = 2;
			}
		}
		if (($turnfound == 2) && ($switchfound2 > 0)) {
			if ($pieces[$turnid][2] > 0) {  // check angle
				$switch = 1;
			}
			else {
				$switch = 2;
			}
		}
		} // switch>0 if
	} // current!=prev if
// end it


// Awareness !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

	$hid = -1;
	$hdist = 0;
	$fid = -1;
	$fdist = 0;
	for ($i=0; $i<$carspositionscount; $i++) {
		$groupmatch = 0;
		if ($i != $posid) {
			if (($current_piece + ($tracklist[$current_piece]['patchmultiplier'] - 1)) < $trackpiecescount) {
			if (($carslist[$i]['piece'] >= $current_piece) && ($carslist[$i]['piece'] <= ($current_piece + $tracklist[$current_piece]['patchmultiplier'] - 1))) {
				$groupmatch = 1;
			}
			}
			else {
			if (($carslist[$i]['piece'] >= $current_piece) || ($carslist[$i]['piece'] <= ($tracklist[$current_piece]['patchmultiplier'] - ($trackpiecescount-$startid) - 1))) {
				$groupmatch = 1;
			}
			}
			$car_distance_left = $tracklist[$carslist[$i]['piece']]['tlength'] - $carslist[$i]['inpiecedist'];
			if (($groupmatch == 1) && ($car_distance_left < $distance_left)) {
				if ($fid >= 0) {
					if ((abs($distance_left - $car_distance_left) < $fdist)) {
									// && ($carslist[$i]['startlane'] == $carslist[$posid]['startlane'])) {
						$fid = $i;
						$fdist = abs($distance_left - $car_distance_left);
					}
				}
				else {
					//if ($carslist[$i]['startlane'] == $carslist[$posid]['startlane']) {
						$fid = $i;
						$fdist = abs($distance_left - $car_distance_left);
					//}
				}
			}
			if (($groupmatch == 1) && ($car_distance_left > $distance_left)) {
				if ($hid >= 0) {
					if ((abs($distance_left - $car_distance_left) < $hdist) && ($carslist[$i]['startlane'] == $carslist[$posid]['startlane'])) {
						$hid = $i;
						$hdist = abs($distance_left - $car_distance_left);
					}
				}
				else {
					if ($carslist[$i]['startlane'] == $carslist[$posid]['startlane']) {
						$hid = $i;
						$hdist = abs($distance_left - $car_distance_left);
					}
				}
			}
		}
	}

// Awareness !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!



		$nturn = 0;					// next turn? turning it into next group turn, ngturn
		$oturn = 0;					// opposite turn?
		$cturn = 0;					// current turn?
		if ($tracklist[$current_piece]['type'] == 1) {		// already in a turn...
			$cturn = 1;
		}
		if ($tracklist[$next_group]['type'] == 1) {		// its turn , slow down?
			$nturn = 1;
			if (($tracklist[$next_group]['angle']*$tracklist[$current_piece]['angle']) < 0) {
				$oturn = 1;			// opposite turn
			}
			elseif (($tracklist[$next_group]['angle']*$tracklist[$current_piece]['angle']) > 0) {
				$oturn = 2;			// same turn
			}
		}



//$txt = $gametick . "\t" . $lap . "\t" . $current_piece . "\t" . $delta . "\t" . $tdistance . "\t" . $carslist[$posid]['angle'] . "\t" . $nturn . "\n";
//fwrite($f, $txt);

	$angle = $carslist[$posid]['angle'];
	$aangle = abs($angle);
	if (($angle * $prev_angle) >= 0) {
		$dangle = abs($angle) - abs($prev_angle);
		$den = max(abs($dangle), abs($prev_angle));
	}
	else {
		$dangle = abs($angle) + abs($prev_angle);
		$den = max(abs($dangle) + abs($prev_angle));
	}

	//$dangle = $angle - $prev_angle;
	$ddangle = $dangle - $prev_dangle; 		// may not work at zero crossing, if both terms have different signs... 
	$dddangle = $ddangle - $prev_ddangle; 		// may not work at zero crossing, if both terms have different signs... 

	if (abs($ddangle) > 0) {	// There is change
		$pddangle = $ddangle / $den;
	}
	else {
		$pddangle = 0;
	}

	if (abs($dddangle) > 0) {	// There is change
		$pdddangle = $dddangle / $den;
	}
	else {
		$pdddangle = 0;
	}

	// *** TO BE DONE *** Angle change rate could be used to determine 45 degree emergency threshold



	$startid = $tracklist[$current_piece]['startid'];
	$lastid = $tracklist[$current_piece]['lastid'];



// check double delta for turbo acceleration

$key = 0;
//$switch = 0;


	if ($cturn == 1) {

		if ($pdistance_elapsed < 0.1) { // sample at 45%
			$aspeed = $delta;
			$throttle = $tracklist[$current_piece]['aspeed']/10;
			$eval = 1;
			$peakangle = $aangle;
		}
		if ($aangle > $peakangle) {
			$peakangle = $aangle;
		}
		if (($pdistance_elapsed > 0.9)) {
			if ((abs($aspeed - $tracklist[$current_piece]['aspeed']) < 1) && ($eval == 1)) { // running at desired aspeed
				$eval = 0;
				if (($peakangle < 50) && ($pddangle <= 0)) {
					$increment = 0.1;
				}
				if (($peakangle < 40) && ($pddangle <= 0)) {
					$increment = 0.3;
				}
				if (($peakangle < 30) && ($pddangle <= 0)) {
					$increment = 0.5;
				}
				if (($peakangle < 20) && ($pddangle <= 0)) {
					$increment = 0.75;
				}
				if (($peakangle < 15) && ($pddangle <= 0)) {
					$increment = 1.0;
				}
				if (($peakangle > 50) && ($pddangle > 0)) {
					$increment = -1.0;
				}


				if (($startid + $tracklist[$current_piece]['patchmultiplier'] - 1) < $trackpiecescount) {
					for ($i=$startid; $i<($startid+$tracklist[$current_piece]['patchmultiplier']-1);$i++) {
						$tspeed = $tracklist[$i]['aspeed'] + $increment;
						if ($tspeed < 2) {
							$tspeed = 2;
						}
						$tracklist[$i]['aspeed'] = $tspeed;
					}
				}
				else {
					for ($i=$startid; $i<$trackpiecescount;$i++) {
						$tspeed = $tracklist[$i]['aspeed'] + $increment;
						if ($tspeed < 2) {
							$tspeed = 2;
						}
						$tracklist[$i]['aspeed'] = $tspeed;
					}
					for ($i=0; $i<($tracklist[$current_piece]['patchmultiplier'] - ($trackpiecescount-$startid));$i++) {
						$tspeed = $tracklist[$i]['aspeed'] + $increment;
						if ($tspeed < 2) {
							$tspeed = 2;
						}
						$tracklist[$i]['aspeed'] = $tspeed;
					}
				}
					// increase our speed as well? or would it be done when comparing aspeed and delta in following code?

			}
		}

		//*** Settle to aspeed at steadystate
		if (abs($delta - $tracklist[$current_piece]['aspeed']) > 1) {
			if ($delta > $tracklist[$current_piece]['aspeed']) {
				if ($delta - $tracklist[$current_piece]['aspeed'] > 3) {
					$throttle = $throttle - 0.2;
				}
				else {
					$throttle = $throttle - 0.1;
				}
			}
			else {
				$throttle = $throttle + 0.1;
			}
		}
		if ($dangle > 2) {
			$throttle = 0.0;
		}


//*****

		if (($pddangle < 6) && ($aangle < 5)) { // compute value...
		 $throttle = $throttle + 0.1; //1
		}
		if ($dangle > 0) {
			$throttle = $throttle - 0.1;
			if ($dangle > 4) {
				$throttle = $throttle - 0.3;
			}
		}
		if ($pddangle > 0) { //why abs? acc is enough, not d of acc
			// if (($dangle < 2) && ($aangle < 10)) {
			if ($aangle < 10) {
				$throttle = $throttle - 0.05;
			}
			else {
				$throttle = $throttle - 0.2;
			}
		}
//*****/






		if ($pdistance_elapsed > 0.7) {
			if (abs($delta - $tracklist[$next_group]['aspeed']) > 1) {
				if ($delta > $tracklist[$next_group]['aspeed']) {
					$throttle = $throttle - 0.1;
				}
				else {
					$throttle = $throttle + 0.1;
				}
			}
		}
		else {
			if (abs($delta - $tracklist[$current_piece]['aspeed']) > 1) {
				if ($delta > $tracklist[$current_piece]['aspeed']) {
					$throttle = $throttle - 0.1;
				}
				else {
					$throttle = $throttle + 0.1;
				}
			}
		}














		//*********************************************************************
		if ($dangle <= 0) { // dangle < 0
/*
			if ($pdistance_elapsed > 0.75) {
				if ($aangle < 40) {
					$throttle = $throttle + 0.2;
				}
				else {
					$throttle = $throttle + 0.05;
				}
			}
			else {// dangle might still be increasing !!! check for dangle instead!
				//$throttle = $throttle + 0.05; 
			}
*/
			// dangerous !! // same as checking if speed is going negative or decreasing in magnitude.
//			if (($pdistance_elapsed > 0.8) && ($delta < 6) && ($turbine == 0) && ($aangle < 40)) {

			if (($pdistance_elapsed > 0.9) && ($turbine == 0) && ($aangle < 40)) {
				if (($nstraightstretch > 200) || ($next_group == $longestid)) {
					$turbothrottle = $tracklist[$next_group]['aspeed']/10 + 0.2; // 0.3 to not waste turbo, turbo or no turbo
					if ($turbo > 0) {
						if ($turbothrottle < 0) {
							$turbothrottle = 0.1;
						}
						elseif ($turbothrottle > 1) {
							$turbothrottle = 1.0;
							$key .= '-';
						}
						//$turbothrottle = $throttle;
						if (abs($delta - (10*$turbothrottle)) > 1) {
							$turbine = 1;
							$key = 't-7';
						}
					}
				}
			}
		}

/*
		if ($pdistance_elapsed > 0.2) {
			if ($dangle < 0) {
				$throttle = $throttle + 0.05;
			}
		}
		if ($pdistance_elapsed > 0.5) {
			if ($dangle < 0) {
				$throttle = $throttle + 0.1; // boooooost
			}
		}
		if ($pdistance_elapsed > 0.5) {
			if (($nstraightstretch > 200) && ($dangle < 0)) {
				$throttle = $throttle + 0.2; // boooooost
			}
		}
		if ($dangle < 0) {
			$throttle = $throttle + 0.05;
		}
*/

		if ($hid >= 0) {
			if (($pdistance_elapsed > 0.8) && ($tracklist[$next_piece]['switch'] == 1) && ($hdist < 150)) {
//*****				if ($carslist[$hid]['ddelta'] > 1) {
					if ($switch == 1) {	// Going right already
						if ($carslist[$posid]['startlane'] < ($tracklanescount - 1)) {
							$switch = 1; // Keep going Right
						}
						else { // Need to switch to left if can't to right, car coming behind!!
							$switch = 2; // Left
						}
					}
					elseif ($switch == 2) {	// Going left already
						if ($carslist[$posid]['startlane'] > 0) {
							$switch = 2; // Keep going Left
						}
						else { // Need to switch to Right if can't to right, car coming behind!!
							$switch = 1; // Right
						}
					}
					else {			// Going straight already, switch to any side !!
						if ($carslist[$posid]['startlane'] > 0) {
							$switch = 2; // Switch left
						}
						else { 
							$switch = 1; // Switch right
						}
					}
					$bach = 1;
//*****				}
			}
		}

/*
		if (($fid >= 0) && ($strike == 0)) { // check lane

			if (($pdistance_elapsed > 0.5) && ($fdist < 120) && ($fdist > 50) && ($aangle < 40) && ($pddangle < 0) && ($distance_left > 200)) {
//				if ($carslist[$fid]['ddelta']  1) {
					$strike = 1;
					$throttle = $tracklist[$next_group]['aspeed']/10 + 0.5;
					//$throttle = 1.0;
					if ($throttle < 0) {
						$throttle = 0.1;
					}
					elseif ($throttle > 1) {
						$throttle = 1.0;
						$key .= '-';
					}
					$turbothrottle = $throttle;
					if ($turbo > 0) {
						$turbine = 1;
						$key = 't-strike';
					}
//				}
			}
		}

*/


		if ($fid >= 0) {
			if (($pdistance_elapsed > 0.8) && ($fdist < 120)) {
				if ($carslist[$fid]['startlane'] != $lane) { // overtaking turbo !!
					$turbothrottle = $tracklist[$next_group]['aspeed']/10 + 0.2;
					//$throttle = 1.0;
					if ($turbothrottle < 0) {
						$turbothrottle = 0.1;
					}
					elseif ($turbothrottle > 1) {
						$turbothrottle = 1.0;
						$key .= '-';
					}
					//$turbothrottle = $throttle;
					if (($turbo > 0) && ($turbine == 0)) {
						$turbine = 1;
						$key = 't-strike';
					}
					else {
						$throttle = 1.0;
					}
					// prevent switching if we might wrongly switch
					if ($carslist[$fid]['endlane'] == $carslist[$posid]['endlane']) { // we're headed the same way??
						if ($carslist[$posid]['startlane'] < $carslist[$posid]['endlane']) { // going right, go left !!
							$switch = 2; // go left
						}
						if ($carslist[$posid]['startlane'] > $carslist[$posid]['endlane']) { // going left, go right !!
							$switch = 1; // go right
						} // even if there's no spare lane, these commands might override the switch request !!
					}
				}
				else {
					// Request switch? he might too? but we're checking at a time when he could've switched if wanted....
//					if ($carslist[$fid]['startlane'] == $carslist[$fid]['endlane']) {
//						$switchrequest = 1;
//					}
//					else {
						if ($carslist[$fid]['endlane'] == $carslist[$posid]['endlane']) { // we're headed the same way??
							if ($carslist[$posid]['startlane'] < $carslist[$posid]['endlane']) { // going right, go left !!
								$switch = 2; // go left
							}
							if ($carslist[$posid]['startlane'] > $carslist[$posid]['endlane']) { // going left, go right !!
								$switch = 1; // go right
							} // even if there's no spare lane, these commands might override the switch request !!
						}
//					}
				}
			}
		}





//		if (($pdistance_elapsed > 0.5) && ($aangle < 35) && ($dangle < 0)) {
//			$throttle = 1.0;
//		}





	$strike = 0;

//-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>-------------------------------------------------------------------<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<--
if (($sliped == 0) && ($aangle > 0)) {
	$aa = $aangle;
	$sliped = 1;
	$bb = ($delta / $tracklist[$current_piece]['radius'])*(180/3.1415);
	$bslip = $bb - $aa;
	$vth = $bslip * (3.1415/180) * $tracklist[$current_piece]['radius'];
	$fslip = $vth * $vth / $tracklist[$current_piece]['radius'];
	$noslipspeed = sqrt($fslip * $tracklist[$current_piece]['radius']);
}
















	}

	// smmooooth turn?? up?
	// incorporate speed calculation here








	if ($nturn == 1) { // next group is turn ?

		if ($pdistance_elapsed < 0.45) {
			$aspeed = $delta;
			$throttle = $tracklist[$current_piece]['aspeed']/10;
		}
		if (($pdistance_elapsed > 0.6) && ($pdistance_elapsed < 0.7)) {
			$throttle = $tracklist[$next_group]['aspeed']/10;
		}
		if ($pdistance_elapsed > 0.7) {
			if (abs($delta - $tracklist[$next_group]['aspeed']) > 1) {
				if ($delta > $tracklist[$next_group]['aspeed']) {
					if (abs($delta - $tracklist[$next_group]['aspeed']) > 2) {
						$throttle = $throttle - 0.2;
					}
					else {
						$throttle = $throttle - 0.1;
					}
				}
				else {
					$throttle = $throttle + 0.05;
				}
			}
		}
		else {
			if (abs($delta - $tracklist[$current_piece]['aspeed']) > 1) {
				if ($delta > $tracklist[$current_piece]['aspeed']) {
					$throttle = $throttle - 0.1;
				}
				else {
					$throttle = $throttle + 0.1;
				}
			}
		}
		if ($pdistance_elapsed > 0.95) {	// braking logic update, might as well update braking distance?
			if (abs($delta - $tracklist[$next_group]['aspeed']) > 1) {
				if ($delta > $tracklist[$next_group]['aspeed']) {
					if (abs($delta - $tracklist[$next_group]['aspeed']) > 2) {
						$tracklist[$current_piece]['aspeed'] = $tracklist[$current_piece]['aspeed'] - 2;
					}
					else {
						$tracklist[$current_piece]['aspeed'] = $tracklist[$current_piece]['aspeed'] - 2;
					}
				}
				else {
					$tracklist[$current_piece]['aspeed'] = $tracklist[$current_piece]['aspeed'] + 0.5;
				}
			}
		}


		$bach = 0;
		if ($hid >= 0) {
			if (($pdistance_elapsed > 0.8) && ($tracklist[$next_piece]['switch'] == 1) && ($hdist < 150)) {
				if ($carslist[$hid]['ddelta'] > 1) {
					if ($switch == 1) {	// Going right already
						if ($carslist[$posid]['startlane'] < ($tracklanescount - 1)) {
							$switch = 1; // Keep going Right
						}
						else { // Need to switch to left if can't to right, car coming behind!!
							$switch = 2; // Left
						}
					}
					elseif ($switch == 2) {	// Going left already
						if ($carslist[$posid]['startlane'] > 0) {
							$switch = 2; // Keep going Left
						}
						else { // Need to switch to Right if can't to left, car coming behind!!
							$switch = 1; // Right
						}
					}
					else {			// Going straight already, switch to any side !!
						if ($carslist[$posid]['startlane'] > 0) {
							$switch = 2; // Switch left
						}
						else { 
							$switch = 1; // Switch right
						}
					}
					$bach = 1;
				}
			}
		}


		if ($fid >= 0) { // check lane
			if (($pdistance_elapsed > 0.5) && ($fdist < 120) && ($fdist > 50) && ($aangle < 40) && ($dangle <= 0) && ($distance_left > 200)) {
//				if ($carslist[$fid]['ddelta']  1) {
				if ($carslist[$fid]['startlane'] == $lane) { // strike !!
					//$strike = 1;
					$throttle = $tracklist[$next_group]['aspeed']/10 + 0.5;
					//$throttle = 1.0;
					if ($throttle < 0) {
						$throttle = 0.1;
					}
					elseif ($throttle > 1) {
						$throttle = 1.0;
						$key .= '-';
					}
					$turbothrottle = $throttle;
					if (($turbo > 0) && ($turbine == 0)) {
						$turbine = 1;
						$key = 't-strike';
					}
					else {
						//$throttle = 1.0; // turn up ahead, can't do that.... dangerous !!
					}
				}
			}
		}



			if (($turbine == 0) && ($aangle < 40) && ($straightstretch > 250)) {
					//$turbothrottle = $tracklist[$next_group]['aspeed']/10 + 0.2; // 0.3 to not waste turbo, turbo or no turbo
					if ($turbo > 0) {
						if ($turbothrottle < 0) {
							$turbothrottle = 0.1;
						}
						elseif ($turbothrottle > 1) {
							$turbothrottle = 1.0;
							$key .= '-';
						}
						//$turbothrottle = $throttle;
						if (abs($delta - (10*$turbothrottle)) > 1) {
							$turbine = 1;
							$key = 't-7';
						}
					}
			}





		if ($delta > 10) { // turbo safe
			if (($pdistance_elapsed > 0.3) && ($ddelta > 0)) {
				$throttle = 0.0;
			}
			if (($pdistance_elapsed > 0.3) && ($ddelta < 0)) {
				$throttle = 0.0;
			}
		}





// could also iterate braking distance

/***
		if (($pdistance_elapsed > 0.3) && ($ddelta < 0) && ($delta < 4)) { // compute target speed
			$throttle = 0.4;
		}
		if  (($distance_left < 200) && ($delta > 8.5)) {
			$throttle = 0.0;
		}
		if  (($distance_left < 150) && ($delta > 7)) {
			$throttle = 0.0;
		}
		if  (($distance_left < 100) && ($delta > 6)) {
			$throttle = 0.0;
		}
		if  (($distance_left < 50) && ($delta > 5)) {
			$throttle = 0.0;
		}
		if  ($distance_left < 20) {
			if ($delta > 5) { // speed
				$throttle = 0.0;
			}
			else {
				$throttle = 0.4;
			}
		}
		if (($straightstretch >= 800) && ($turbine == 0)) {
			if ($turbo > 0) {
				$turbothrottle = 0.7;
				if (abs($delta - (10*$turbothrottle)) > 0) {
					$turbine = 1;
					$key = 't-1';
				}
			}
		}

		if (($straightstretch >= 500) && ($lap == ($totallaps - 1)) && ($turbine == 0)) { // compare to target optimal speed
			if ($turbo > 0) {
				$turbothrottle = 0.5; // compute speed needed, then use turbine
				if (abs($delta - (10*$turbothrottle)) > 0) {
					$turbine = 1;
					$key = 't-2';
				}
			}
		}

		if ($delta > 10) {
			$throttle = $throttle - 0.1;
		}

***/

	}



// Overtake































	// make turbo period expirable, and check if the stretch is near or not... if near, wait, else use it !!!


	// compute dynamically?
	if ($delta < 5) {
//		$throttle = $throttle + 0.1; //1
	}


	/*
	delta speed incorporated above i think, this not needed!
	// TURBINE THROTTLE
	$deltathrottle = $throttle - $prev_throttle;		// are we performing high throttle?
	if (($deltathrottle > 0.4) && ($turbo > 0) && ($cturn == 0) && ($straightstretch > 200) && ($turbine == 0)) { // && cturn == 0, nturn == 0?
		$turbothrottle = $throttle;	// use turbo
		$turbine = 1;
		$key = '
t-y';
	}
	*/

	if ($delta < 2) { // recovering crash
//		$throttle = $throttle + 0.1;
	}

	if ($throttle < 0) {
		$throttle = 0.05;
	}
	elseif ($throttle > 1) {
		$throttle = 1.0;
		$key .= '-';
	}

//-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>-------------------------------------------------------------------<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<--
//$switch = 0;
//$throttle = 0.2;



if ($pieces[$current_piece][0] == 0) {
$txt = $gametick . "\t" . $lap . "\t" . $current_piece . "\t" . substr($delta,0,9) . "\t\t" . substr($carslist[$posid]['angle'],0,9) . "\t\t" . $throttle . "\t\t" . $key . "\t\t" . substr($pdistance_elapsed,0,4)*100 . "%" . "\t\t" . $aspeed . "\t" . $tspeed . "\n";
}
else {
$txt = $gametick . "\t" . $lap . "\t" . $current_piece . "-\t" . substr($delta,0,9) . "\t\t" . substr($carslist[$posid]['angle'],0,9) . "\t\t" . $throttle . "\t\t" . $key . "\t\t" . substr($pdistance_elapsed,0,4)*100 . "%" . "\t\t" . $aspeed . "\t" . $tspeed . "\n";
}

//print_r($tracklist);
	fwrite($f, $txt);
	//$throttle = 0.4;

	echo "Lap:\t\t\t" . $lap . "\t\t\t";
	echo "Turbo:\t\t" . $turbo . "\t" . "-" . "\t" . $turbined . "\r\n";

	if ($pieces[$current_piece][0] == 1) {
		echo "Piece:\t\t\t" . $current_piece . "-\t\t\t";
	}
	else {
		echo "Piece:\t\t\t" . $current_piece . "\t\t\t";
	}	
	echo "Turbine:\t" . $turbine . "\r\n";

	echo "Speed:\t\t\t" . $delta . "\r\n";
	echo "ASpeed:\t\t\t" . $tracklist[$current_piece]['aspeed'] . "\r\n";
	echo "NASpeed:\t\t" . $tracklist[$next_group]['aspeed'] . "\r\n";
	echo "INcrement:\t\t" . $increment . "\r\n";
	echo "Peak angle:\t\t" . $peakangle . "\r\n";

//	echo "long:\t\t\t" . $longestid . "\r\n";
//	echo "long:\t\t\t" . $longestlength . "\r\n";

	echo "Angle:\t\t\t" . $angle . "\r\n";
//	echo "hdist:\t\t\t" . $hdist . "-" . $hid . "\r\n";
//	echo "fdist:\t\t\t" . $fdist . "-" . $fid . "\r\n";
	echo "FID:\t\t\t" . $fid . "\r\n";
	echo "Here>>>" . $here . "\r\n";
	echo "FDst:\t\t\t" . $fdist . "\r\n";
	echo "TImer:\t\t\t" . $turbotimer . "\r\n";
	echo "Switchable?:\t\t\t" . $tracklist[$next_piece]['switch'] . "\r\n";
	echo "Throttle:\t\t" . $throttle . "\r\n";
	echo ">>>>>>>:\t\t" . $inpiecedist . "%\r\n";
//	echo ">>>>>>>:\t\t" . substr($pdistance_elapsed*100,0,4) . "%\r\n";


	if ($turbine == 2) {
		$this->write_msg('turbo', 'Turbineeeeeeeeeeeeee !!');
		$turbo = $turbo - 1;
		fwrite($f, "\n" . "Turbine !!" . "\n");
		$turbined = $turbined + 1;
		$turbotimer = $turboduration + 3;
		$turbine = 3;
	}
	else {
		if (($switch == 1) && ($prev_command != "switch")) {
			$prev_command = "switch";
			$this->write_msg('switchLane', 'Right');
			$switched = 1;
			$switch = 0;
		}
		elseif (($switch == 2) && ($prev_command != "switch")) {
			$prev_command = "switch";
			$this->write_msg('switchLane', 'Left');
			$switched = 1;
			$switch = 0;
		}
		else {
			$prev_command = "throttle";
			if ($turbine == 1) {
				$throttle = $turbothrottle;
				$turbine = 2; // call in turbo
			}
//			$this->write_msg('throttle', $throttle);
			$this->write_msg('throttle', $throttle, $gametick);
		}
	}

	if (($turbotimer == 0) && ($turbine == 3)) {
		$turbotimer = 0;
		$turbine = 0;
	}
	if ($turbotimer > 0) {
		$turbotimer = $turbotimer - 1;
	}


	fwrite($this->file, "\n\n" . "THROTTLE = " . $throttle . "\n\n");

	$this->throttle = $throttle;
	$this->prevangle = $carslist[$posid]['angle'];

$prev_hspeed = $delta;

$prev_delta = $delta;
$prev_angle = $angle;
$prev_dangle = $dangle;
$prev_ddangle = $ddangle;
$prev_piece = $current_piece;
$prev_throttle = $throttle;
$ping = 0;
	break;

	// ******************************************************************************

	case "turboAvailable":

	$turbo = $turbo + 1;
	$turboduration = $msgd['data']['turboDurationTicks']; // ticks

	break;


	// ******************************************************************************

	case "gameEnd":
	echo "*** Its Game End";
	echo "\r\n";

	echo "Results count:	";
	echo $resultscount = count($msgd['data']['results']);
	$results = $msgd['data']['results'];
	echo "\r\n";
	echo "Best laps:		";
	echo $bestlapscount = count($msgd['data']['bestLaps']);
	$bestlaps = $msgd['data']['bestLaps'];
	echo "\r\n";

	// Could log race results !!

	break;

	// ******************************************************************************

	case "tournamentEnd":
	echo "*** Its Tournament End";
	echo "\r\n";

	fclose($f);

	break;

	// ******************************************************************************

	case "crash":
	echo "*** Its Crash !!!";
	echo "\r\n";

	echo "Game ID:			";
	echo $gameid = $msgd['gameId'];
	echo "\r\n";
	echo "Game tick:		";
	echo $gametick = $msgd['gameTick'];
	echo "\r\n";

	echo "Name:				";
	echo $crashname = $msgd['data']['name'];
	echo "\r\n";
	echo "Color:			";
	echo $crashcolor = $msgd['data']['color'];
	echo "\r\n";
	
	break;

	// ******************************************************************************

	case "spawn":
	echo "*** Its Spawn !!!";
	echo "\r\n";

	echo "Game ID:			";
	echo $gameid = $msgd['gameId'];
	echo "\r\n";
	echo "Game tick:		";
	echo $gametick = $msgd['gameTick'];
	echo "\r\n";

	echo "Name:				";
	echo $spawnname = $msgd['data']['name'];
	echo "\r\n";
	echo "Color:			";
	echo $spawncolor = $msgd['data']['color'];
	echo "\r\n";

	if (($spawnname == $this->myname) && ($spawncolor == $this->mycolor)) {
		$throttle = 1.0;
	}

	break;
	
	// ******************************************************************************

	case "lapFinished":
	echo "*** Its Lap Finished !!!";
	echo "\r\n";

	echo "Game ID:			";
	echo $gameid = $msgd['gameId'];
	echo "\r\n";
	echo "Game tick:		";
	echo $gametick = $msgd['gameTick'];
	echo "\r\n";

	echo "Name:				";
	echo $lapfcarname = $msgd['data']['car']['name'];
	echo "\r\n";
	echo "Color:			";
	echo $lapfcolor = $msgd['data']['car']['color'];
	echo "\r\n";

	echo "* LAP TIME:		";
	echo "\r\n";
	echo "Lap:				";
	echo $lapfltlap = $msgd['data']['lapTime']['lap'];
	echo "\r\n";
	echo "Ticks:			";
	echo $lapfltticks = $msgd['data']['lapTime']['ticks'];
	echo "\r\n";
	echo "Millis:			";
	echo $lapfltmillis = $msgd['data']['lapTime']['millis']/1000;
	echo "\r\n";

	echo "* RACE TIME:		";
	echo "\r\n";
	echo "Laps:				";
	echo $lapfrtlaps = $msgd['data']['raceTime']['laps'];
	echo "\r\n";
	echo "Ticks:			";
	echo $lapfrtticks = $msgd['data']['raceTime']['ticks'];
	echo "\r\n";
	echo "Millis:			";
	echo $lapfrtmillis = $msgd['data']['raceTime']['millis']/1000;
	echo "\r\n";
	
	echo "* RANKING:		";
	echo "\r\n";
	echo "Overall:			";
	echo $lapflap = $msgd['data']['ranking']['overall'];
	echo "\r\n";
	echo "Fastest Lap:		";
	echo $lapfticks = $msgd['data']['ranking']['fastestLap'];
	echo "\r\n";
	
	break;
	
	// ******************************************************************************
	
	case "dnf":
	echo "*** Its disqualified";
	echo "\r\n";

	echo "Game ID:			";
	echo $gameid = $msgd['gameId'];
	echo "\r\n";
	echo "Game tick:		";
	echo $gametick = $msgd['gameTick'];
	echo "\r\n";

	echo "Name:				";
	echo $dnfname = $msgd['data']['car']['name'];
	echo "\r\n";
	echo "Color:			";
	echo $dnfcolor = $msgd['data']['car']['color'];
	echo "\r\n";

	echo "Reason:			";
	echo $dnfreason = $msgd['data']['reason'];
	echo "\r\n";

	break;
	
	// ******************************************************************************
	
	case "finish":
	echo "*** Its FINISH !";
	echo "\r\n";

	echo "Game ID:			";
	echo $gameid = $msgd['gameId'];
	echo "\r\n";
	echo "Game tick:		";
	echo $gametick = $msgd['gameTick'];
	echo "\r\n";

	echo "Name:				";
	echo $finishname = $msgd['data']['name'];
	echo "\r\n";
	echo "Color:			";
	echo $finishcolor = $msgd['data']['color'];
	echo "\r\n";

	break;
	
	// ******************************************************************************
	

	
		
	default:
	echo "nothing to do";
}




/*

			switch ($msg['msgType']) {
				case 'carPositions':
					$this->write_msg('throttle', 1.0);
					break;
				case 'join':
				case 'yourCar':
				case 'gameInit':
				case 'gameStart':
				case 'crash':
				case 'spawn':
				case 'lapFinished':
				case 'dnf':
				case 'finish':
				default:
					$this->write_msg('ping', null);
			}

*/
			//$this->write_msg('ping', null);
if ($ping == 1) {
			$this->write_msg('ping', null);
}
$ping = 1;
		}
	}
}
?>
