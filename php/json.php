<?php

$msg1 = '

{"msgType": "gameInit", "data": {
  "race": {
    "track": {
      "id": "indianapolis",
      "name": "Indianapolis",
      "pieces": [
        {
          "length": 100.0
        },
        {
          "length": 100.0,
          "switch": true
        },
        {
          "radius": 200,
          "angle": 22.5
        }
      ],
      "lanes": [
        {
          "distanceFromCenter": -20,
          "index": 0
        },
        {
          "distanceFromCenter": 0,
          "index": 1
        },
        {
          "distanceFromCenter": 20,
          "index": 2
        }
      ],
      "startingPoint": {
        "position": {
          "x": -340.0,
          "y": -96.0
        },
        "angle": 90.0
      }
    },
    "cars": [
      {
        "id": {
          "name": "Schumacher",
          "color": "red"
        },
        "dimensions": {
          "length": 40.0,
          "width": 20.0,
          "guideFlagPosition": 10.0
        }
      },
      {
        "id": {
          "name": "Rosberg",
          "color": "blue"
        },
        "dimensions": {
          "length": 40.0,
          "width": 20.0,
          "guideFlagPosition": 10.0
        }
      }
    ],
    "raceSession": {
      "laps": 3,
      "maxLapTimeMs": 30000,
      "quickRace": true
    }
  }
}}

';

$msg2 = '
{"msgType": "yourCar", "data": {
  "name": "Schumacher",
  "color": "red"
}}
';

$msg3 = '
{"msgType": "carPositions", "data": [
  {
    "id": {
      "name": "Schumacher",
      "color": "red"
    },
    "angle": 0.0,
    "piecePosition": {
      "pieceIndex": 0,
      "inPieceDistance": 0.0,
      "lane": {
        "startLaneIndex": 0,
        "endLaneIndex": 0
      },
      "lap": 0
    }
  },
  {
    "id": {
      "name": "Rosberg",
      "color": "blue"
    },
    "angle": 45.0,
    "piecePosition": {
      "pieceIndex": 0,
      "inPieceDistance": 20.0,
      "lane": {
        "startLaneIndex": 1,
        "endLaneIndex": 1
      },
      "lap": 0
    }
  }
], "gameId": "OIUHGERJWEOI", "gameTick": 0}

';

$msg4 = '
{"msgType": "gameEnd", "data": {
  "results": [
    {
      "car": {
        "name": "Schumacher",
        "color": "red"
      },
      "result": {
        "laps": 3,
        "ticks": 9999,
        "millis": 45245
      }
    },
    {
      "car": {
        "name": "Rosberg",
        "color": "blue"
      },
      "result": {}
    }
  ],
  "bestLaps": [
    {
      "car": {
        "name": "Schumacher",
        "color": "red"
      },
      "result": {
        "lap": 2,
        "ticks": 3333,
        "millis": 20000
      }
    },
    {
      "car": {
        "name": "Rosberg",
        "color": "blue"
      },
      "result": {}
    }
  ]
}}
';



$msg5 = '
{"msgType": "crash", "data": {
  "name": "Rosberg",
  "color": "blue"
}, "gameId": "OIUHGERJWEOI", "gameTick": 3}
';

$msg6 = '
{"msgType": "spawn", "data": {
  "name": "Rosberg",
  "color": "blue"
}, "gameId": "OIUHGERJWEOI", "gameTick": 150}
';


$msg7 = '
{"msgType": "lapFinished", "data": {
  "car": {
    "name": "Schumacher",
    "color": "red"
  },
  "lapTime": {
    "lap": 2,
    "ticks": 666,
    "millis": 6660
  },
  "raceTime": {
    "laps": 1,
    "ticks": 888,
    "millis": 9990
  },
  "ranking": {
    "overall": 1,
    "fastestLap": 1
  }
}, "gameId": "OIUHGERJWEOI", "gameTick": 300}
';


$msg8 = '

{"msgType": "dnf", "data": {
  "car": {
    "name": "Rosberg",
    "color": "blue"
  },
  "reason": "disconnected"
}, "gameId": "OIUHGERJWEOI", "gameTick": 650}

';

$msg9 = '

{"msgType": "finish", "data": {
  "name": "Schumacher",
  "color": "red"
}, "gameId": "OIUHGERJWEOI", "gameTick": 2345}

';

$msg10 = '
{"msgType": "join", "data": {
  "name": "Schumacher",
  "key": "UEWJBVNHDS"
}}
';

$msg = $msg10;


function otr($obj) {
	if (!is_object($obj) && !is_array($obj)) {
		return $obj;
	}
	if (is_object($obj)) {
		$obj = get_object_vars($obj);
	}
	return array_map("otr", $obj);
}

$msged = str_replace(" ", "", preg_replace("/\s+/", " ", $msg));

//echo $msged;
//echo "<br />";

$msgd = otr(json_decode($msged));


print_r ($msgd);

switch ($msgd["msgType"]) {
	
	// ******************************************************************************
	
	case "join":
	echo "*** Its FINISH !";
	echo "<br /><br />\r\n";

	echo "Name:				";
	echo $joinname = $msgd['data']['name'];
	echo "<br />\r\n";
	echo "Color:			";
	echo $joinkey = $msgd['data']['key'];
	echo "<br />\r\n";

	break;
	
	// ******************************************************************************
	
	case "gameInit":
	echo "*** its init";
	echo "<br /><br />\r\n";
	
	echo "Track ID:			";
	echo $trackid = $msgd['data']['race']['track']['id'];
	echo "<br />\r\n";
	echo "Track name:		";
	echo $trackname = $msgd['data']['race']['track']['name'];
	echo "<br />\r\n";
	echo "Track pieces:		";
	echo $trackpiecescount = count($msgd['data']['race']['track']['pieces']);
	$trackpieces = $msgd['data']['race']['track']['pieces'];
	echo "<br />\r\n";
	echo "Track lanes:		";
	echo $tracklanescount = count($msgd['data']['race']['track']['lanes']);
	$tracklanes = $msgd['data']['race']['track']['lanes'];
	
	echo "<br />\r\n";
	echo "Starting pointx:	";
	echo $trackstartingpointx = $msgd['data']['race']['track']['startingPoint']['position']['x'];
	echo "<br />\r\n";
	echo "Starting pointy:	";
	echo $trackstartingpointy = $msgd['data']['race']['track']['startingPoint']['position']['y'];
	echo "<br />\r\n";
	echo "Starting angle:	";
	echo $trackstartingpointangle = $msgd['data']['race']['track']['startingPoint']['angle'];
	echo "<br />\r\n";
	echo "Cars:				";
	echo $carscount = count($msgd['data']['race']['cars']);
	$cars = $msgd['data']['race']['cars'];
	echo "<br />\r\n";
	echo "Laps:				";
	echo $totallaps = $msgd['data']['race']['raceSession']['laps'];
	echo "<br />\r\n";
	echo "Max time limit:	";
	echo $maxlaptime = $msgd['data']['race']['raceSession']['maxLapTimeMs'];
	echo "<br />\r\n";
	echo "Quick race?:		";
	echo $quickrace = $msgd['data']['race']['raceSession']['quickRace'];
	echo "<br />\r\n";
	
	break;

	// ******************************************************************************
		
	case "yourCar":
	echo "*** Its Your Car";
	echo "<br /><br />\r\n";
	
	echo "Car name:			";
	echo $carname = $msgd['data']['name'];
	echo "<br />\r\n";
	echo "Car Color:		";
	echo $carcolor = $msgd['data']['color'];
	
	break;

	// ******************************************************************************

	case "gameStart":
	echo "*** Its Game Start !!";
	echo "<br /><br />\r\n";
	break;

	// ******************************************************************************

	case "carPositions":
	echo "*** Its Car Positions";
	echo "<br /><br />\r\n";
	
	echo "Game ID:			";
	echo $gameid = $msgd['gameId'];
	echo "<br />\r\n";
	echo "Game tick:		";
	echo $gametick = $msgd['gameTick'];
	echo "<br />\r\n";

	echo "Count data Cars:	";
	echo $carspositionscount = count($msgd['data']);
	$carspositions = $msgd['data'];
	echo "<br />\r\n";
	
	// Extract personal car position, extract other car position;
	
	echo "<br />\r\n";

	break;

	// ******************************************************************************

	case "gameEnd":
	echo "*** Its Game End";
	echo "<br /><br />\r\n";

	echo "Results count:	";
	echo $resultscount = count($msgd['data']['results']);
	$results = $msgd['data']['results'];
	echo "<br />\r\n";
	echo "Best laps:		";
	echo $bestlapscount = count($msgd['data']['bestLaps']);
	$bestlaps = $msgd['data']['bestLaps'];
	echo "<br />\r\n";
	
	echo "Track pieces:		";
	echo $trackpiecescount = count($msgd['data']['race']['track']['pieces']);
	$trackpieces = $msgd['data']['race']['track']['pieces'];
	echo "<br />\r\n";




	break;

	// ******************************************************************************

	case "tournamentEnd":
	echo "*** Its Tournament End";
	echo "<br /><br />\r\n";
	break;

	// ******************************************************************************

	case "crash":
	echo "*** Its Crash !!!";
	echo "<br /><br />\r\n";

	echo "Game ID:			";
	echo $gameid = $msgd['gameId'];
	echo "<br />\r\n";
	echo "Game tick:		";
	echo $gametick = $msgd['gameTick'];
	echo "<br />\r\n";

	echo "Name:				";
	echo $crashname = $msgd['data']['name'];
	echo "<br />\r\n";
	echo "Color:			";
	echo $crashcolor = $msgd['data']['color'];
	echo "<br />\r\n";
	
	break;

	// ******************************************************************************

	case "spawn":
	echo "*** Its Spawn !!!";
	echo "<br /><br />\r\n";

	echo "Game ID:			";
	echo $gameid = $msgd['gameId'];
	echo "<br />\r\n";
	echo "Game tick:		";
	echo $gametick = $msgd['gameTick'];
	echo "<br />\r\n";

	echo "Name:				";
	echo $spawnname = $msgd['data']['name'];
	echo "<br />\r\n";
	echo "Color:			";
	echo $spawncolor = $msgd['data']['color'];
	echo "<br />\r\n";

	break;
	
	// ******************************************************************************

	case "lapFinished":
	echo "*** Its Lap Finished !!!";
	echo "<br /><br />\r\n";

	echo "Game ID:			";
	echo $gameid = $msgd['gameId'];
	echo "<br />\r\n";
	echo "Game tick:		";
	echo $gametick = $msgd['gameTick'];
	echo "<br />\r\n";

	echo "Name:				";
	echo $lapfcarname = $msgd['data']['car']['name'];
	echo "<br />\r\n";
	echo "Color:			";
	echo $lapfcolor = $msgd['data']['car']['color'];
	echo "<br />\r\n";

	echo "* LAP TIME:		";
	echo "<br />\r\n";
	echo "Lap:				";
	echo $lapfltlap = $msgd['data']['lapTime']['lap'];
	echo "<br />\r\n";
	echo "Ticks:			";
	echo $lapfltticks = $msgd['data']['lapTime']['ticks'];
	echo "<br />\r\n";
	echo "Millis:			";
	echo $lapfltmillis = $msgd['data']['lapTime']['millis'];
	echo "<br />\r\n";

	echo "* RACE TIME:		";
	echo "<br />\r\n";
	echo "Laps:				";
	echo $lapfrtlaps = $msgd['data']['raceTime']['laps'];
	echo "<br />\r\n";
	echo "Ticks:			";
	echo $lapfrtticks = $msgd['data']['raceTime']['ticks'];
	echo "<br />\r\n";
	echo "Millis:			";
	echo $lapfrtmillis = $msgd['data']['raceTime']['millis'];
	echo "<br />\r\n";
	
	echo "* RANKING:		";
	echo "<br />\r\n";
	echo "Overall:			";
	echo $lapflap = $msgd['data']['ranking']['overall'];
	echo "<br />\r\n";
	echo "Fastest Lap:		";
	echo $lapfticks = $msgd['data']['ranking']['fastestLap'];
	echo "<br />\r\n";
	
	break;
	
	// ******************************************************************************
	
	case "dnf":
	echo "*** Its disqualified";
	echo "<br /><br />\r\n";

	echo "Game ID:			";
	echo $gameid = $msgd['gameId'];
	echo "<br />\r\n";
	echo "Game tick:		";
	echo $gametick = $msgd['gameTick'];
	echo "<br />\r\n";

	echo "Name:				";
	echo $dnfname = $msgd['data']['car']['name'];
	echo "<br />\r\n";
	echo "Color:			";
	echo $dnfcolor = $msgd['data']['car']['color'];
	echo "<br />\r\n";

	echo "Reason:			";
	echo $dnfreason = $msgd['data']['reason'];
	echo "<br />\r\n";

	break;
	
	// ******************************************************************************
	
	case "finish":
	echo "*** Its FINISH !";
	echo "<br /><br />\r\n";

	echo "Game ID:			";
	echo $gameid = $msgd['gameId'];
	echo "<br />\r\n";
	echo "Game tick:		";
	echo $gametick = $msgd['gameTick'];
	echo "<br />\r\n";

	echo "Name:				";
	echo $finishname = $msgd['data']['name'];
	echo "<br />\r\n";
	echo "Color:			";
	echo $finishcolor = $msgd['data']['color'];
	echo "<br />\r\n";

	break;
	
	// ******************************************************************************
	

	
		
	default:
	echo "nothing to do";
}


/*
foreach ($s as $val) {
	echo $val . "<br />";
	echo key($s) . "<br />";	
}
*/
echo "<br /><br /><br />";
















/*
$a = array("o1", "o2");
$arr["aaa"] = "msgType";
$arr["rr"]["a"] = $a;
$arr["rr"]["b"] = "hieeee";
$x = json_encode($arr);
echo $x;
echo "<br /><br /><br />";
$y = json_decode($x);
$s = otr($y);
print_r($s);//var_dump($s);
*/
















?>
